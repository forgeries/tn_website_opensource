import request from '@/utils/request'

const api_prefix = process.env.VUE_APP_BASE_API_PREFIX

export function getBannerPosList(params) {
  return request({
    url: api_prefix + 'banner_pos/list',
    method: 'get',
    params
  })
}

export function getBannerPosByID(id) {
  return request({
    url: api_prefix + 'banner_pos/get_id',
    method: 'get',
    params: { id }
  })
}

export function getBannerPosAllTitle() {
  return request({
    url: api_prefix + 'banner_pos/get_all_title',
    method: 'get'
  })
}

export function addBannerPos(data) {
  return request({
    url: api_prefix + 'banner_pos/add',
    method: 'post',
    data
  })
}

export function editBannerPos(data) {
  return request({
    url: api_prefix + 'banner_pos/edit',
    method: 'put',
    data
  })
}

export function deleteBannerPos(ids) {
  return request({
    url: api_prefix + 'banner_pos/delete',
    method: 'delete',
    data: { ids }
  })
}
