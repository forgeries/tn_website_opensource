<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-04-09
 * Time: 13:57
 */
declare(strict_types = 1);

use think\facade\Route;

Route::group(function () {
    // 管理员
    Route::group(function () {
        Route::post('admin/login','Admin@login'); // 管理员登陆
    });

    // 验证token
    Route::group(function () {
        // 管理员
        Route::group('admin', function () {
            Route::post('logout', 'logout'); // 管理员退出登陆
            Route::post('add', 'addAdmin'); // 添加管理员
            Route::put('edit', 'editAdmin'); // 编辑管理员
            Route::put('update', 'updateAdmin')->middleware(['super_admin']); // 更新管理员
            Route::delete('delete', 'deleteAdmin')->middleware(['super_admin']); // 删除管理员

            Route::get('routes','routes'); // 获取管理员路由信息
            Route::get('info','info'); // 获取管理员信息
            Route::get('list', 'getList'); // 获取管理员列表数据
            Route::get('get_id', 'getByID'); // 根据id获取管理员信息
        })->prefix('Admin/');

        // 权限角色
        Route::group('auth_role', function () {
            Route::post('add','addRole'); // 添加权限角色信息
            Route::put('edit','editRole'); // 编辑权限角色信息
            Route::put('update', 'updateRole'); // 更新权限角色信息
            Route::delete('delete', 'deleteRole'); // 删除权限角色信息
            Route::get('list','getList'); // 获取权限角色信息
            Route::get('all', 'getAll'); // 获取全部权限角色信息
            Route::get('get_id', 'getByID'); // 根据id获取具体权限信息
        })->prefix('AuthRole/');

        // 权限角色菜单
        Route::group('auth_menu', function () {
            Route::post('add', 'addMenu'); // 添加角色权限菜单信息
            Route::put('edit', 'editMenu'); // 编辑角色权限菜单信息
            Route::put('update', 'updateMenu'); // 更新角色权限菜单信息
            Route::delete('delete', 'deleteMenu'); // 删除角色权限菜单信息
            Route::get('table_tree', 'getTableTree'); // 获取角色权限菜单表格树形数据
            Route::get('element_tree', 'getElementTree'); // 获取角色权限菜单树形数据
            Route::get('get_id', 'getByID'); // 根据id获取角色权限菜单信息
            Route::get('all_menu', 'getAllMenuNode'); // 获取角色权限菜单的所有节点信息
            Route::get('get_children_count', 'getChildrenCount'); // 获取角色权限菜单对应父节点下子节点的数量
        })->prefix('AuthMenu/');

        // 权限角色规则
        Route::group('auth_rule', function () {
            Route::post('add', 'addRule'); // 添加角色权限规则信息
            Route::put('edit', 'editRule'); // 编辑角色权限规则信息
            Route::put('update', 'updateRule'); // 更新角色权限规则信息
            Route::delete('delete', 'deleteRule'); // 删除角色权限规则信息
            Route::get('table_tree', 'getTableTree'); // 获取角色权限规则表格树形数据
            Route::get('element_tree', 'getElementTree'); // 获取角色权限规则树形数据
            Route::get('get_id', 'getByID'); // 根据id获取角色权限规则信息
            Route::get('all_rule', 'getAllRuleNode'); // 获取角色权限规则的所有节点信息
            Route::get('get_children_count', 'getChildrenCount'); // 获取角色权限规则对应父节点下子节点的数量
        })->prefix('AuthRule/');

        // 系统配置项
        Route::group('system_config', function () {
            Route::post('add', 'addConfig'); // 添加系统配置项信息
            Route::post('commit_config', 'commitConfigData'); // 更新系统配置信息
            Route::put('edit', 'editConfig'); // 编辑系统配置项信息
            Route::put('update', 'updateConfig'); // 更新系统配置项信息
            Route::delete('delete', 'deleteConfig'); // 删除系统配置项信息
            Route::get('table_tree', 'getTableTree'); // 获取系统配置项Table树形数据
            Route::get('get_id', 'getByID'); // 根据id获取系统配置项的信息
            Route::get('get_config_menu', 'getMenuData'); // 获取系统配置菜单信息
            Route::get('get_all_parent', 'getAllConfigParentNode'); // 获取系统配置所有父节点信息
            Route::get('get_children_count', 'getChildrenCount'); // 获取系统配置对应父节点下子节点的数量
        })->prefix('SystemConfig/');

        // 系统日志
        Route::group('system_log', function() {
            Route::delete('delete', 'deleteLog'); // 删除日志信息
            Route::delete('clear_all', 'clearAllLog'); // 清空日志信息
            Route::get('list', 'getList'); // 获取日志列表数据
        })->prefix('SystemLog/');

        // 图集类目分类
        Route::group('atlas_category', function () {
            Route::post('add', 'addAtlasCategory'); // 添加图集类目分类信息
            Route::put('edit', 'editAtlasCategory'); // 编辑图集类目分类信息
            Route::put('update', 'updateAtlasCategory'); // 更新图集类目分类信息
            Route::delete('delete', 'deleteAtlasCategory'); // 删除图集类目分类信息
            Route::get('list', 'getList'); // 获取图集类目分类的分页列表数据
            Route::get('get_by_id', 'getByID'); // 根据id获取图集类目分类的数据
        })->prefix('AtlasCategory/');

        // 图集图片管理
        Route::group('atlas', function () {
            Route::put('change_category', 'changeCategory'); // 修改指定图片对应的栏目
        })->prefix('Atlas/');

        // 微信配置项
        Route::group('wx_config', function () {
            Route::post('add', 'addConfig'); // 添加微信配置项信息
            Route::post('commit_config', 'commitConfigData'); // 更新微信配置信息
            Route::put('edit', 'editConfig'); // 编辑微信配置项信息
            Route::put('update', 'updateConfig'); // 更新微信配置项信息
            Route::delete('delete', 'deleteConfig'); // 删除微信配置项信息
            Route::get('table_tree', 'getTableTree'); // 获取微信配置项Table树形数据
            Route::get('get_id', 'getByID'); // 根据id获取微信配置项的信息
            Route::get('get_config_menu', 'getMenuData'); // 获取微信配置菜单信息
            Route::get('get_all_parent', 'getAllConfigParentNode'); // 获取微信配置所有父节点信息
            Route::get('get_children_count', 'getChildrenCount'); // 获取微信配置对应父节点下子节点的数量
        })->prefix('WxConfig/');

        // 轮播位
        Route::group('banner_pos', function () {
            Route::post('add', 'addBannerPos'); // 添加轮播位信息
            Route::put('edit', 'editBannerPos'); // 编辑轮播位信息
            Route::delete('delete', 'deleteBannerPos'); // 删除轮播位信息
            Route::get('list', 'getList'); // 获取轮播位的分页数据
            Route::get('get_id', 'getByID'); // 根据id获取轮播位信息
            Route::get('get_all_title', 'getAllTitle'); // 获取全部的轮播位名称
        })->prefix('BannerPos/');

        // 轮播图
        Route::group('banner', function () {
            Route::post('add', 'addBanner'); // 添加轮播图信息
            Route::put('edit', 'editBanner'); // 编辑轮播图信息
            Route::put('update', 'updateBanner'); // 更新轮播图信息
            Route::delete('delete', 'deleteBanner'); // 删除轮播图信息
            Route::get('list', 'getList'); // 获取轮播图的分页数据
            Route::get('get_id', 'getByID'); // 根据id获取轮播图信息
            Route::get('get_banner_count', 'getBannerCountByPosID'); // 获取指定轮播位的轮播图数量
        })->prefix('Banner/');

        // 微信用户
        Route::group('wechat_user', function () {
            Route::put('update', 'updateWeChatUser'); // 更新微信用户的信息
            Route::get('list', 'getList'); // 获取微信用户的分页数据
        })->prefix('WeChatUser/');

        // 栏目
        Route::group('category', function () {
            Route::post('add', 'addCategory'); // 添加栏目信息
            Route::put('edit', 'editCategory'); // 编辑栏目信息
            Route::put('update', 'updateCategory'); // 更新栏目信息
            Route::delete('delete', 'deleteCategory'); // 删除栏目信息
            Route::get('table_tree', 'getTableTree'); // 获取栏目的表格树形数据
            Route::get('get_id', 'getByID'); // 根据id获取栏目信息
            Route::get('get_children_count', 'getChildrenCount'); // 获取指定栏目下的子元素数量
            Route::get('get_all_top', 'getTopNode'); // 获取全部的顶级的栏目节点信息
            Route::get('get_all', 'getAllNode'); // 获取全部栏目的信息
        })->prefix('Category/');

        // 业务
        Route::group('business', function () {
            Route::post('add', 'addBusiness'); // 添加业务信息
            Route::put('edit', 'editBusiness'); // 编辑业务信息
            Route::put('update', 'updateBusiness'); // 更新业务信息
            Route::delete('delete', 'deleteBusiness'); // 删除业务信息
            Route::get('list', 'getList'); // 获取业务的分页数据
            Route::get('get_id', 'getByID'); // 根据id获取对应的业务信息
            Route::get('get_operation_user', 'getOperationUser'); // 获取业务指定的操作用户的分页数据
        })->prefix('Business/');

        // 模型
        Route::group('model_table', function () {
            Route::post('add', 'addModelTable'); // 添加模型信息
            Route::put('edit', 'editModelTable'); // 编辑模型信息
            Route::put('update', 'updateModelTable'); // 更新模型信息
            Route::delete('delete', 'deleteModelTable'); // 删除模型信息
            Route::get('list', 'getList'); // 获取模型的分页数据
            Route::get('get_id', 'getByID'); // 根据id获取对应的模型信息
            Route::get('get_all_name', 'getAllName'); // 获取全部模型的名称
            Route::get('get_fields_data', 'getFieldsData'); // 获取模型字段数据
        })->prefix('ModelTable/');

        // 内容
        Route::group('content', function () {
            Route::post('add', 'addContent'); // 添加内容信息
            Route::put('edit', 'editContent'); // 编辑内容信息
            Route::put('update', 'updateContent'); // 更新内容信息
            Route::delete('delete', 'deleteContent'); // 删除内容信息
            Route::get('list', 'getList'); // 获取内容的分页数据
            Route::get('get_id', 'getByID'); // 根据id获取对应的内容信息
            Route::get('get_category_children', 'getCategoryChildrenCount'); // 根据栏目获取对应内容的数量
            Route::get('get_operation_user', 'getOperationUser'); // 获取内容指定的操作用户的分页数据
        })->prefix('Content/');

    })->middleware(['admin.role']);

    Route::get('/','Index@index');
    Route::get('index/default_config', 'Index@getDefaultConfig'); // 获取后台的默认配置信息
})
    ->prefix('\\app\\admin\\controller\\')
    ->middleware(['record.log'])
    ->allowCrossDomain();