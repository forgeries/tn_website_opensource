<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-06
 * Time: 17:30
 */

namespace app\admin\model;


use think\model\Pivot;

class AuthRoleRule extends Pivot
{
    protected $autoWriteTimestamp = false;
}