<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-04-09
 * Time: 17:04
 */

namespace app\common\exception;


class BaseException extends \Exception
{
    // HTTP 状态码
    public $code = 400;
    // 错误具体信息
    public $msg = '参数错误';
    // 自定义错误码
    public $errorCode = 10000;

    public function __construct(array $params = [])
    {
        // 判断参数是否为数组
        if (!is_array($params)) {
            return '';
        }

        // 判断参数是否存在，如果存在则替换为新值
        if (array_key_exists('code', $params)) {
            $this->code = $params['code'];
        }
        if (array_key_exists('msg', $params)) {
            $this->msg = $params['msg'];
        }
        if (array_key_exists('errorCode', $params)) {
            $this->errorCode = $params['errorCode'];
        }
    }
}