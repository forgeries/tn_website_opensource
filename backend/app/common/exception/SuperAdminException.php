<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-04-30
 * Time: 10:08
 */

namespace app\common\exception;


class SuperAdminException extends BaseException
{
    public $code = 403;
    public $msg = '当前被操作用户为超级管理员，不允许该操作';
    public $errorCode = 60001;
}