<?php


namespace app\common\exception;


class ResultException extends BaseException
{
    public $code = 404;
    public $msg = '搜索结果为空';
    public $errorCode = 10011;
}