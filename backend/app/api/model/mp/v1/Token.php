<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-06-02
 * Time: 16:22
 */

namespace app\api\model\mp\v1;

use app\api\validate\mp\v1\Token as Validate;
use app\common\exception\ParameterException;
use app\common\model\MpApiUserToken;

class Token
{
    /**
     * 根据code生成生成token
     * @param $code
     * @return string
     */
    public static function getToken($code)
    {
        $validate = new Validate();
        if (!$validate->scene('get')->check(['code' => $code])) {
            throw new ParameterException([
                'msg' => $validate->getError(),
            ]);
        }

        // 获取token
        return (new MpApiUserToken($code))->getUserToken();
    }

    /**
     * 验证token是否存在
     * @param $token
     * @return bool
     */
    public static function verifyToken($token)
    {
        $validate = new Validate();
        if (!$validate->scene('verify')->check(['token' => $token])) {
            throw new ParameterException([
                'msg' => $validate->getError(),
            ]);
        }

        return MpApiUserToken::verifyToken($token);
    }
}